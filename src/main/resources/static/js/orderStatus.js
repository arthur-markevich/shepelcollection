
function sendData(btn) {
    var theTD = btn.parentNode;
    var theTR = theTD.parentNode;
    var brothers = theTR.childNodes;

    fire_ajax_submit(brothers);
}

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
}



function fire_ajax_submit(brothers) {

    var rowData = brothers;
    var orderItem = {}; // main object

    // for (var i = 0; i < rowData.length; i++) {
    //     console.log(i, rowData[i].nodeName)
    // }

   console.log("product.id", rowData[1].value);
   console.log("quantity", rowData[9].childNodes[1].value);

    orderItem["product"] =  parseInt(rowData[1].value, 10);
    orderItem["quantity"] = parseInt(rowData[9].childNodes[1].value, 10);

    var options = [];

    var options_ = rowData[7].childNodes;
    var k;
    for (k = 0; k < options_.length; k++) {
        var node = options_[k];
        if (node.nodeType == Node.ELEMENT_NODE && node.nodeName == 'SELECT') {
        console.log(k + ' ==> ', node.nodeName + ' id: ' + node.value);
            var option_id = parseInt(node.value, 10);
                if (option_id > 0) {
                    options.push(option_id);
                }
        }
    }

    // console.log("options created", options);

    orderItem["price"] = 0;
    orderItem["options"] = options;

    $("#btn-add").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/orders/add",
        data: JSON.stringify(orderItem),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var orderDetails = "positions: <b>" + data.count + "</b> price: <b>" + data.amount + "</b> "
            + "<a href='" + getContextPath() + "/order' >" + "View order</a>";

            $('#orderDetails').html(orderDetails);

            console.log("SUCCESS : ", data);
            $("#btn-add").prop("disabled", false);

        },

        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btn-add").prop("disabled", false);

        }
    });

}