package com.shepelcollection.shepel.repository;

import com.shepelcollection.shepel.model.Addition;
import com.shepelcollection.shepel.model.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TypeRepository extends JpaRepository<Type, Long> {

    Optional<Type> findByName(String name);

//    List<Addition> findAllByAdditionsIsNotContainingAnd

}
