package com.shepelcollection.shepel.repository;

import com.shepelcollection.shepel.model.Category;
import com.shepelcollection.shepel.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByName(String name);

    Optional<Product> findById(Long id);

    List<Product> findAllByCategoryName(String category);

    List<Product> findAllByCategoryId(Long id);

    List<Product> findAll();
}
