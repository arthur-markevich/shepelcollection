package com.shepelcollection.shepel.repository;

import com.shepelcollection.shepel.model.Role;
import com.shepelcollection.shepel.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(RoleName roleName);
}
