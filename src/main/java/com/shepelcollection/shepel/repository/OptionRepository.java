package com.shepelcollection.shepel.repository;


import com.shepelcollection.shepel.model.Option;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OptionRepository extends JpaRepository<Option, Long> {

    List<Option> findAllByAddition_Id(Long id);

}
