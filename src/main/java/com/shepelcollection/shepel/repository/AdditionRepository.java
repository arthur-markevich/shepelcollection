package com.shepelcollection.shepel.repository;

import com.shepelcollection.shepel.model.Addition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdditionRepository extends JpaRepository<Addition, Long> {

    Optional<Addition> findByName(String name);

//    List<Addition> findAllByTypeId(Long typeId);


}
