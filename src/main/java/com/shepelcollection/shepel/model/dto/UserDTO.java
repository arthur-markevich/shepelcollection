package com.shepelcollection.shepel.model.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

public class UserDTO {

    @NotBlank
    @Size(max = 40)
    @NotEmpty(message = "*Please provide your name")
    private String name;

    @NotEmpty
    @NotBlank
    @Size(max = 16)
    private String username;

    @Length(min = 5, message = "*Your password must have at least 5 characters")
    @NotEmpty(message = "*Please provide your password")
    @Size(max = 100)
    private String password;
    private String matchingPassword;

    @NotNull
    @Size(max =40)
    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserDTO() {

    }
}
