package com.shepelcollection.shepel.model.dto;

import java.math.BigDecimal;

public class OptionDTO {

    private Long id;

    private BigDecimal price;

    public OptionDTO() {

    }

    public OptionDTO(Long id, BigDecimal price) {
        this.id = id;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
