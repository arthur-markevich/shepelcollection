package com.shepelcollection.shepel.model.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class AdditionForm {

    @NotBlank
    @Size(max = 40)
    @Column(unique = true)
    @NotEmpty(message = "*Please provide name")
    private String name;

    private Long typeId;

    public AdditionForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }
}
