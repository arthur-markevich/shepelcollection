package com.shepelcollection.shepel.model;

import com.shepelcollection.shepel.model.audit.IdAudit;
import org.hibernate.annotations.Fetch;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Entity
@Table(name = "options")
public class Option extends IdAudit {

    @NotBlank
    @Size(max = 40)
    @NotEmpty(message = "*Please provide name")
    private String name;


    @Column(nullable = false, precision = 7, scale = 2)
    @DecimalMin(value = "0.1", inclusive = true)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal price;

    @Size(max = 250)
    @Column(name = "img_uri")
    private String img;

    @Size(max = 750)
    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    private Addition addition;



    public Option() {

    }

    public Option(long id) {
        setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Addition getAddition() {
        return addition;
    }

    public void setAddition(Addition addition) {
        this.addition = addition;
    }
}
