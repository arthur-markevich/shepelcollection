package com.shepelcollection.shepel.model;

import java.math.BigDecimal;

public class AjaxResponseOrder {

    private int count;

    private BigDecimal amount;



    public AjaxResponseOrder() {

    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
