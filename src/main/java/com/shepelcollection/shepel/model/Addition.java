package com.shepelcollection.shepel.model;

import com.shepelcollection.shepel.model.audit.IdAudit;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "addition")
public class Addition extends IdAudit {

    @NotBlank
    @Size(max = 40)
    @Column(unique = true)
    @NotEmpty(message = "*Please provide name")
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "addition_id")
    @Column(nullable = false)
    private Set<Option> options = new HashSet<>();


//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "product_type_id")
//    private Type type;

    @ManyToMany(mappedBy = "additions")
    private Set<Type> types;

    public Addition() {
    }

    public Set<Type> getTypes() {
        return types;
    }

    public void setTypes(Set<Type> types) {
        this.types = types;
    }

    public void addType(Type type) {
        types.add(type);
    }

    public Addition(long id) {
        setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Option> getOptions() {
        return options;
    }

    public void setOptions(Set<Option> options) {
        this.options = options;
    }

//    public Type getType() {
//        return type;
//    }
//
//    public void setType(Type type) {
//        this.type = type;
//    }
}
