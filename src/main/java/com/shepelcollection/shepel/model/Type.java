package com.shepelcollection.shepel.model;

import com.shepelcollection.shepel.model.audit.IdAudit;
import groovy.lang.Lazy;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;


@Entity
@Table(name = "product_type")
public class Type extends IdAudit {

    @NotBlank
    @Size(max = 40)
    @Column(unique = true)
    @NotEmpty(message = "*Please provide name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_type_id")
    private Set<Product> products = new LinkedHashSet<>();


//    @OneToMany(fetch = FetchType.EAGER)
//    @JoinColumn(name = "product_type_id")

//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "type_additions",
//    joinColumns = @JoinColumn(name = "type_id"),
//    inverseJoinColumns = @JoinColumn(name = "addition_id"))
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Addition> additions = new LinkedHashSet<>();


    public boolean addAddition(Addition addition) {
        return this.additions.add(addition);
    }

    public Type(Long id) {
        this.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Addition> getAdditions() {
        return additions;
    }

    public void setAdditions(Set<Addition> additions) {
        this.additions = additions;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Type() {

    }
}
