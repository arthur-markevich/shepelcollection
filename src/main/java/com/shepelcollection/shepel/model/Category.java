package com.shepelcollection.shepel.model;

import com.shepelcollection.shepel.model.audit.IdAudit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "category")
public class Category extends IdAudit {

    @NotNull
    @Size(max = 100)
    @Column(unique = true)
    private String name;

    public Category(long id) {
        setId(id);
    }

    public Category() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
