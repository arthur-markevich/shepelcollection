package com.shepelcollection.shepel.web;

import com.shepelcollection.shepel.exception.BadRequestException;
import com.shepelcollection.shepel.model.Addition;
import com.shepelcollection.shepel.model.Type;
import com.shepelcollection.shepel.repository.AdditionRepository;
import com.shepelcollection.shepel.repository.TypeRepository;
import com.shepelcollection.shepel.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TypeController {

    @Autowired
    TypeRepository typeRepository;

    @Autowired
    TypeService typeService;


    @Autowired
    AdditionRepository additionRepository;

    @GetMapping("/types/create")
    public String createTypeForm(Model model) {
        model.addAttribute("type", new Type());
        model.addAttribute("types", typeRepository.findAll());
        return  "admin/type_form";
    }

    @PostMapping("/types/create")
    public String saveType(@Valid Type type, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (!bindingResult.hasErrors()) {
            Type typeExists = typeRepository.findByName(type.getName()).orElse(new Type(-1L));
            if (typeExists.getId() < 0) {
                typeRepository.save(type);
                redirectAttributes.addFlashAttribute("flash.message", "New Type was successfully saved");
            } else {
                String error = "Type with name '" + type.getName() +"' already exists";
                redirectAttributes.addFlashAttribute("flash.error", error);
            }

        }

        return "redirect:/types/create";
    }


    @GetMapping("/types/{typeId}")
    public  String getOneType(@PathVariable(name = "typeId") Long typeId, Model model) {
        Type type = typeRepository.findById(typeId).orElse(new Type(-1L));

        if (type.getId() > 0) {
            List<Addition> additions = additionRepository.findAll();

            model.addAttribute("type", type);
            model.addAttribute("types", typeRepository.findAll());
            model.addAttribute("additions", typeService.getAllLeftAdditions(type, additions));
        } else {
            throw new BadRequestException("Type not found");
        }
        return "admin/type_single";
    }

    @PostMapping("/types/add_addition")
    public String addAddition(@RequestParam(value = "type.id", required = true) Long typeId,
                              @RequestParam(value = "addition.id", required = true) Long additionId,
                              RedirectAttributes redirectAttributes) {
        Type type = typeService.findById(typeId).orElse(new Type(-1L));
        Addition addition = additionRepository.findById(additionId).orElse(new Addition(-1L));

        if(type.getId() < 0 || addition.getId() < 0) {
            throw new BadRequestException("Wrong parameters");
        }

       boolean add = type.addAddition(addition);
        if (add) {
            typeRepository.save(type);
            redirectAttributes.addFlashAttribute("flash.message", addition.getName() + " successfully added");
        } else {
            redirectAttributes.addFlashAttribute("flash.error", type.getName() + " already contains " + addition.getName());
        }
        return "redirect:/types/" + typeId;
    }

    @PostMapping("/types/delete_addition/{additionId:[\\d]+}")
    public String deleteAddition(@PathVariable("additionId") Long additionId, @RequestParam(value = "type.id", required = true) Long typeId, RedirectAttributes redirectAttributes) {

        Type type = typeService.findById(typeId).orElse(new Type(-1l));
        Addition addition = additionRepository.findById(additionId).orElse(new Addition(-1L));

        if(type.getId() < 0 || addition.getId() < 0) {
            throw new BadRequestException("Wrong parameters");
        }

        boolean removed = type.getAdditions().remove(addition);
        if (removed) {
            typeRepository.save(type);
            redirectAttributes.addFlashAttribute("flash.message", addition.getName() + " successfully deleted");
        } else {
            redirectAttributes.addFlashAttribute("flash.error", "Something went wrong");
        }

        return "redirect:/types/" + type.getId();
    }

    @PostMapping("/types/{typeId}")
    public  String updateType(@Valid Type type,@PathVariable(name = "typeId") Long typeId , BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        Type typeExists = typeRepository.findById(typeId).orElse(new Type(-1L));
        if (typeExists.getId() > 0) {
            typeExists.setName(type.getName());
            typeRepository.save(typeExists);
            redirectAttributes.addFlashAttribute("flash.message", "Successful!");
        } else {
            redirectAttributes.addFlashAttribute("flash.error", "Failed to save new Product Type");
        }
        return "redirect:/types/" + typeId;
    }

    @PostMapping("/types/delete/{typeId:[\\d]+}")
    public String deleteType(@PathVariable(name = "typeId") Long typeId, RedirectAttributes redirectAttributes) {
        Type type = typeRepository.findById(typeId).orElse(new Type(-1L));
        if (type.getId() > 0) {
            typeRepository.delete(type);
            redirectAttributes.addFlashAttribute("flash.message", type.getName() + " was successfully deleted!");
        } else {
            redirectAttributes.addFlashAttribute("flash.error", "Wrong request");
        }

        return "redirect:/types/create";
    }
}
