package com.shepelcollection.shepel.web;

import com.shepelcollection.shepel.config.AppConstants;
import com.shepelcollection.shepel.exception.BadRequestException;
import com.shepelcollection.shepel.exception.ResourceNotFoundException;
import com.shepelcollection.shepel.model.*;
import com.shepelcollection.shepel.model.dto.OptionDTO;
import com.shepelcollection.shepel.repository.TypeRepository;
import com.shepelcollection.shepel.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TypeRepository typeRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private OptionService optionService;


    @GetMapping("/products")
    public String fullList(Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("products", productService.findAll());
        return "product/index";
    }

    @GetMapping("/categories/{categoryId:[\\d]+}/products")
    public String getAllProductsByCategoryId(@PathVariable(value = "categoryId") Long categoryId, Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("products", productService.findAllByCategoryId(categoryId));
        return "product/index";
    }

    @GetMapping("/products/{id:[\\d]+}")
    public String getOneProduct(@PathVariable(value = "id") Long id, Model model) {
        Product product = productService.findOne(id).orElseThrow(() -> new ResourceNotFoundException("Product", "id", id));
        model.addAttribute("product", product);
        return "product/single";
    }

    @GetMapping("/{category}")
    public String getAllProductsByCategory(@PathVariable(value = "category") String category, Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("products", productService.findAllByCategory(category));
        return "product/index";
    }

    @GetMapping("/products/create")
    public String addProductPage(Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("types", typeRepository.findAll());
        model.addAttribute("product", new Product());

        return "admin/create_product";
    }

    @PostMapping("/products/create")
    public String addProduct(@Valid Product product, BindingResult bindingResult, @RequestParam("image") MultipartFile file, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            for (Object object : bindingResult.getAllErrors()) {
                if (object instanceof FieldError) {
                    FieldError fieldError = (FieldError) object;

                    System.out.println(fieldError.getCode());
                }

                if (object instanceof ObjectError) {
                    ObjectError objectError = (ObjectError) object;

                    System.out.println(objectError.getCode());
                }
            }
        }

        Type type = typeRepository.findById(product.getType().getId()).orElse(new Type(-1L));
        Category category = categoryService.findById(product.getCategory().getId()).orElse(new Category(-1L));
        if (type.getId() > 0 && category.getId() > 0) {
            String imgUri = AppConstants.NO_IMG_URI;

            if (file != null && !file.isEmpty()) {
                fileStorageService.isImage(file);
                imgUri = fileStorageService.store(file, "images", type.getName());
            }

            try {
                product.setImg(imgUri);
                product.setCreatedAt(Instant.now());
                productService.saveProduct(product);
                redirectAttributes.addFlashAttribute("flash.message", "Product created successfully!");
            } catch (Exception e) {
                e.printStackTrace();
                redirectAttributes.addFlashAttribute("flash.error", e.getMessage());
            }

        } else {
            redirectAttributes.addFlashAttribute("flash.error", "Bad request");
        }

        return "redirect:/products/create";
    }

    @GetMapping("/products/{productId:[\\d]+}/edit")
    public String getEditProductUi(@PathVariable(name = "productId") Long productId, Model model) {
        Product product = productService.findOne(productId).orElse(new Product(-1L));
        if (product.getId() < 0) {
            throw new BadRequestException("Wrong request");
        }
        model.addAttribute("product", product);
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("types", typeRepository.findAll());
        return "admin/edit_product";
    }

    @PostMapping("/products/save")
    public String changeProduct(@Valid Product product, @RequestParam("image") MultipartFile file, RedirectAttributes redirectAttributes) {
        Type type = typeRepository.findById(product.getType().getId()).orElse(new Type(-1L));
        Category category = categoryService.findById(product.getCategory().getId()).orElse(new Category(-1L));
        Product existsProduct = productService.findOne(product.getId()).orElse(new Product(-1L));

        if (existsProduct.getId() < 0 || type.getId() < 0 || category.getId() < 0) {
            throw new BadRequestException("Requested product not found");
        }

        String imgUri = existsProduct.getImg();

        if (file != null && !file.isEmpty()) {
            fileStorageService.isImage(file);
            if (imgUri != null && !imgUri.equals(AppConstants.NO_IMG_URI) && !imgUri.startsWith("http")) {
                fileStorageService.delete(imgUri);
            }
            imgUri = fileStorageService.store(file, "images", existsProduct.getType().getName());
        }
        product.setImg(imgUri);
        productService.saveProduct(product);
        redirectAttributes.addFlashAttribute("flash.message", "Successful!");

        return "redirect:/products/" + existsProduct.getId() + "/edit";
    }

    @PostMapping("/products/delete")
    public String deleteProduct(@RequestParam("id") String productId, RedirectAttributes redirectAttributes) {
        long id = -1L;
        try {
            id = Long.valueOf(productId);
        } catch (NumberFormatException e) {
            throw new BadRequestException("Wrong request parameters");
        }
        Product product = productService.findOne(id).orElse(new Product(-1L));
        if (product.getId() < 0) {
            throw new BadRequestException("Product not found");
        }
        productService.delete(product);
        redirectAttributes.addFlashAttribute("flash.message", "Product successfully deleted!");
        return "redirect:/" + product.getCategory().getName();
    }

    @GetMapping("/test")
    public String testTable(Model model) {
        List<Option> optionsList = optionService.findAll();

        List<OptionDTO> optionDTOS = new ArrayList<>();
        optionsList.forEach(o -> {
            OptionDTO opDto = new OptionDTO(o.getId(), o.getPrice());
            optionDTOS.add(opDto);
        });


        model.addAttribute("products", productService.findAll());
        model.addAttribute("options", optionDTOS);

        return "product/table_test";
    }


}
