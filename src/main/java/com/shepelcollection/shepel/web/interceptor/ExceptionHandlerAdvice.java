package com.shepelcollection.shepel.web.interceptor;

import com.shepelcollection.shepel.exception.BadRequestException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleMaxSizeException(
            MaxUploadSizeExceededException ex,
            RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("flash.error", "Upload File is too large");
        return "redirect:/products/create";
    }

    @ExceptionHandler(BadRequestException.class)
    public String handleBadRequestException(BadRequestException ex,
                                           Model model) {


        String errorMessage = ex.getMessage();
        model.addAttribute("error", errorMessage);
        return "error/error_404" ;
    }



}
