package com.shepelcollection.shepel.web;

import com.shepelcollection.shepel.model.User;
import com.shepelcollection.shepel.model.dto.UserDTO;
import com.shepelcollection.shepel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;


    @GetMapping("/")
    public String root() {
        return "index";
    }

    @GetMapping("/user")
    public String userIndex() {
        return "user/index";
    }

    @GetMapping("/admin")
    public String adminIndex() {
        return "admin/index";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "error/access-denied";
    }

    @GetMapping("/registration")
    public String signup(Model model) {
        model.addAttribute("user", new UserDTO());
        return "registration";
    }

    @PostMapping("/registration")
    public String createNewUser(@Valid UserDTO user, BindingResult bindingResult, Model model) {
        User userExits = userService.findUserByEmail(user.getEmail()).orElse(new User(-1L));

        if (!user.getPassword().equals(user.getMatchingPassword())) {
            bindingResult
                    .rejectValue("matchingPassword", "error.user",
                            "The passwords is not match");
        }

        if (userExits.getId() > 0) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        //  //  //  //  //  //  //  //  //  //  //  //  //  //
//        for (Object object : bindingResult.getAllErrors()) {
//            if(object instanceof FieldError) {
//                FieldError fieldError = (FieldError) object;
//
//                System.out.println(fieldError.getCode());
//            }
//
//            if(object instanceof ObjectError) {
//                ObjectError objectError = (ObjectError) object;
//
//                System.out.println(objectError.getCode());
//            }
//        }
        //  //  //  //  //  //  //  //  //  //  //  //  //  //

        User registered = new User();

        if (!bindingResult.hasErrors()) {
            registered.setCreatedAt(Instant.now());
            registered.setUpdatedAt(Instant.now());
            registered.setEnabled(true);
            registered.setName(user.getName());
            registered.setUsername(user.getUsername());
            registered.setEmail(user.getEmail());
            registered.setPassword(user.getPassword());

            userService.saveUser(registered);
            model.addAttribute("successMessage", "User has been registered successfully");
        }
        model.addAttribute("user", user);

        return "registration";
    }

}
