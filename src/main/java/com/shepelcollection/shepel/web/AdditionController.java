package com.shepelcollection.shepel.web;

import com.shepelcollection.shepel.model.Addition;
import com.shepelcollection.shepel.model.Option;
import com.shepelcollection.shepel.model.Type;
import com.shepelcollection.shepel.model.dto.AdditionForm;
import com.shepelcollection.shepel.repository.AdditionRepository;
import com.shepelcollection.shepel.repository.TypeRepository;
import com.shepelcollection.shepel.service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class AdditionController {

    @Autowired
    private AdditionRepository additionRepository;

    @Autowired
    private TypeRepository typeRepository;

    @Autowired
    private OptionService optionService;


    @GetMapping("/additions/create")
    public String createAdditionForm(Model model) {
        model.addAttribute("additionForm", new AdditionForm());
//        model.addAttribute("types", typeRepository.findAll());
        model.addAttribute("additions", additionRepository.findAll());
        return  "admin/addition_form";
    }

    @PostMapping("/additions/create")
    public String addAddition(@Valid AdditionForm additionForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (!bindingResult.hasErrors()) {
//            Type type = typeRepository.findById(additionForm.getTypeId()).orElse(new Type(-1L));
            Addition additionExists = additionRepository.findByName(additionForm.getName()).orElse(new Addition(-1L));

            if (additionExists.getId() < 0) {
                Addition addition = new Addition();
                addition.setName(additionForm.getName());
//                addition.addType(type);
                additionRepository.save(addition);
                redirectAttributes.addAttribute("message", "Successful");

            } else {
                redirectAttributes.addAttribute("error", "Addition '" + additionForm.getName() + "' already exists");
            }

        } else {
            redirectAttributes.addAttribute("error", "Something is went wrong");
        }

        return "redirect:/additions/create";
    }


    @PostMapping("/additions/{additionId:[\\d]+}")
    public String updateAddition(@Valid Addition addition, @PathVariable(name = "additionId") Long additionId, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

//        Type type = typeRepository.findById(addition.getType().getId()).orElse(new Type(-1L));


        if (!bindingResult.hasErrors()) {
            Addition additionExists = additionRepository.findById(additionId).orElse(new Addition(-1L));
            if(additionExists.getId() > 0) {
                additionExists.setName(addition.getName());
//                additionExists.setType(type);
                additionRepository.save(additionExists);
                redirectAttributes.addAttribute("message", "Successful!");

            } else {
                String error = "Addition with name '" + addition.getName() +"' didn't exists";
                redirectAttributes.addAttribute("error", error);
            }

        } else {
            redirectAttributes.addAttribute("error", "Bad request!");
        }


        return "redirect:/additions/" + additionId;
    }

    @PostMapping("/additions/delete/{additionId:[\\d]+}")
    public String deleteAdditio(@PathVariable(name = "additionId") Long additionId, RedirectAttributes redirectAttributes) {
        Addition addition = additionRepository.findById(additionId).orElse(new Addition(-1L));
        if (addition.getId() > 0) {
            additionRepository.delete(addition);
            redirectAttributes.addAttribute("message", addition.getName() + " was successfully deleted!");
        } else {
            redirectAttributes.addAttribute("error", "Wrong request");
        }

        return "redirect:/additions/create";
    }

    @GetMapping("/additions/{additionId:[\\d]+}")
    public String getOne(@PathVariable(name = "additionId") Long additionId, Model model) {
        Addition addition = additionRepository.findById(additionId).orElse(new Addition(-1L));
        if (addition.getId() > 0) {
            model.addAttribute("addition", addition);
        } else {
            model.addAttribute("error", "Bad request!");
        }
        model.addAttribute("option", new Option());
        model.addAttribute("options", optionService.findAllByAdditionId(additionId));
//        model.addAttribute("types", typeRepository.findAll());
        model.addAttribute("additions", additionRepository.findAll());
        return "admin/addition_single";
    }

    @GetMapping("/additions/{typeName}/all")
    public String getAllByTypeName(@PathVariable(name = "typeName") String typeName, Model model) {
        Type type = typeRepository.findByName(typeName).orElse(new Type(-1L));
        if (type.getId() > 0) {
            model.addAttribute("type", type);
//            model.addAttribute("additions", additionRepository.findAllByTypeId(type.getId()));
        } else {
            model.addAttribute("error", "Type '" + typeName + "' not found");
        }
        return "admin/additions";
    }

}
