package com.shepelcollection.shepel.web;

import com.shepelcollection.shepel.exception.BadRequestException;
import com.shepelcollection.shepel.model.*;
import com.shepelcollection.shepel.model.dto.OptionDTO;
import com.shepelcollection.shepel.service.HelperService;
import com.shepelcollection.shepel.service.OptionService;
import com.shepelcollection.shepel.service.OrderService;
import com.shepelcollection.shepel.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OptionService optionService;

    @Autowired
    private ProductService productService;



    @GetMapping("/order")
    public String getCurrentOrder(Model model, HttpSession session) {

        List<Option> optionsList = optionService.findAll();

        List<OptionDTO> optionDTOS = new ArrayList<>();
        optionsList.forEach(o -> {
            OptionDTO opDto = new OptionDTO(o.getId(), o.getPrice());
            optionDTOS.add(opDto);
        });

        model.addAttribute("options", optionDTOS);
        return "order/session_order";
    }

    @PostMapping("/order/edit")
    public String editOrder(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String errorMessage = "Bad request during editing row item";

        HttpSession session = request.getSession();
        List<OrderItem> currentOrder = (List<OrderItem>) session.getAttribute("currentOrder");
        if (currentOrder == null) {
            throw new BadRequestException(errorMessage);
        }

        Map<String,String[]> params = request.getParameterMap();

        final OrderItem[] orderItem = new OrderItem[1];
        Set<Option> options = new LinkedHashSet<>();
        final BigDecimal[] sum = {BigDecimal.ZERO};
        params.forEach((k,v) -> {
            if (k.equals("item.index")) {
                Long index = v.length > 0 ? HelperService.getValue(v[0]) : -1L;
                if (index < 0) {
                    throw new BadRequestException(errorMessage + ": Wrong \"id\" format.");
                }

                orderItem[0] = currentOrder.get(index.intValue());

                orderItem[0].setProduct(orderItem[0].getProduct());
                sum[0] = sum[0].add(orderItem[0].getProduct().getPriceFabric());
            } else if (k.equals("quantity")) {
                Long quantity = v.length > 0 ? HelperService.getValue(v[0]) : -1L;
                if (quantity < 0) {
                    throw new BadRequestException(errorMessage + ": quantity format is wrong.");
                }
                orderItem[0].setQuantity(quantity);
            } else if (k.startsWith("addition_")) {
                Long optionId = v.length > 0 ? HelperService.getValue(v[0]) : -1L;
                if (optionId < 0) {
                    throw new BadRequestException(errorMessage + ": Option is wrong");
                }
                if (optionId == 0) {
                    return;
                }

                Option option = optionService.findOne(optionId).orElse(new Option(-1L));
                if (option.getId() < 0) {
                    throw new BadRequestException(errorMessage + ": Wrong options.");
                }
                sum[0] = sum[0].add(option.getPrice());
                options.add(option);
            }
        });
        sum[0] = sum[0].multiply(new BigDecimal(orderItem[0].getQuantity()));
        orderItem[0].setPrice(sum[0]);
        orderItem[0].setOptions(options);

        AjaxResponseOrder orderDetails = orderService.getOrderDetails(currentOrder);
        session.setAttribute("orderDetails", orderDetails);
        session.setAttribute("currentOrder", currentOrder);

        return "redirect:/order";
    }

    @PostMapping("/order/delete")
    public String deleteOrderItem(HttpServletRequest request) {
        String indexStr = request.getParameter("item.index");
        Long index = HelperService.getValue(indexStr);
        if (index < 0) {
            throw new BadRequestException("Wrong index");
        }
        HttpSession session = request.getSession();
        List<OrderItem> currentOrder = (List<OrderItem>) session.getAttribute("currentOrder");
        if (currentOrder == null) {
            throw new BadRequestException("No order found");
        }
        currentOrder.remove(index.intValue());

        AjaxResponseOrder orderDetails = orderService.getOrderDetails(currentOrder);
        session.setAttribute("orderDetails", orderDetails);
        session.setAttribute("currentOrder", currentOrder);

        return "redirect:/order";
    }

    @PostMapping("/order/save")
    public String saveOrder(@RequestParam(name = "title") String title,HttpSession session, RedirectAttributes redirectAttributes) {
        if (session == null) {
            throw new BadRequestException("Session is null");
        }
        List<OrderItem> orderItems = (List<OrderItem>) session.getAttribute("currentOrder");

        Set<OrderItem> set = new LinkedHashSet<>(orderItems);

        set.forEach(i -> System.out.println(i.getProduct().getName()));

        if (title != null && !title.isEmpty()) {
            Order order = new Order();
            order.setTitle(title);
            order.setOrderItems(set);
            order.setCreatedAt(Instant.now());
            order.setUpdatedAt(Instant.now());
            orderService.save(order);

            session.removeAttribute("currentOrder");
            session.removeAttribute("orderDetails");
            redirectAttributes.addFlashAttribute("flash.message", "Order saves successfully");
        }

        return "redirect:/test";
    }

    @GetMapping("/orders")
    public String getOrders(Model model) {
        model.addAttribute("orders", orderService.findAll());

        return "order/orders";
    }

    @GetMapping("/orders/{orderId:[\\d]+}")
    public String getOrder(@PathVariable(name = "orderId") Long orderId, Model model) {
        Order order = orderService.findById(orderId).orElse(new Order(-1L));
        if (order.getId() < 0) {
            throw new BadRequestException("Order not found");
        }
        model.addAttribute("order", order);

        return "order/single_order";
    }

    @PostMapping("/orders/add")
    public ResponseEntity<?> addToCart(@Valid @RequestBody OrderItem item, Errors errors, HttpSession session) {
        if (session == null) {
            throw new BadRequestException("Sorry, session not found, try again");
        }

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(e -> System.out.println(e.getDefaultMessage()));
            new BadRequestException("Something went wrong");
        }

        Set<Option> options = new LinkedHashSet<>();

        Long productId = item.getProduct().getId();
        Long quantity = item.getQuantity();

        if (quantity < 1) {
            throw new BadRequestException("Quantity must be grater then zero");
        }

        Product product = productService.findOne(productId).orElse(new Product(-1L));
        if (product.getId() < 0) {
            throw new BadRequestException("wrong id parameter");
        }

        BigDecimal sum = product.getPriceFabric();

        for (Option op : item.getOptions()) {
            Option option = optionService.findOne(op.getId()).orElseThrow(() -> new BadRequestException("Wrong option id"));
            if (option.getPrice().compareTo(BigDecimal.ZERO) > 0) {
                sum = sum.add(option.getPrice());
            } else {
                System.out.println("option price error --> " + option.getPrice());
            }
            options.add(option);
        }

        sum = sum.multiply(new BigDecimal(quantity));

        OrderItem orderItem = new OrderItem();
        orderItem.setPrice(sum);
        orderItem.setQuantity(quantity);
        orderItem.setProduct(product);
        orderItem.setOptions(options);

        // add to session
        List<OrderItem> currentOrder = (List<OrderItem>) session.getAttribute("currentOrder");
        if (currentOrder != null) {
            currentOrder.add(orderItem);
        } else {
            currentOrder = new LinkedList<>();
            currentOrder.add(orderItem);
            session.setAttribute("currentOrder", currentOrder);
        }


        AjaxResponseOrder result = orderService.getOrderDetails(currentOrder);

        session.setAttribute("orderDetails", result);

        return ResponseEntity.ok(result);
    }


}
