package com.shepelcollection.shepel.web;

import com.shepelcollection.shepel.config.AppConstants;
import com.shepelcollection.shepel.exception.BadRequestException;
import com.shepelcollection.shepel.model.Addition;
import com.shepelcollection.shepel.model.Option;
import com.shepelcollection.shepel.repository.AdditionRepository;
import com.shepelcollection.shepel.service.FileStorageService;
import com.shepelcollection.shepel.service.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;

@Controller
public class OptionController {

    @Autowired
    private OptionService optionService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private AdditionRepository additionRepository;



    @PostMapping("/options/add")
    public String addOption(@Valid Option option, @RequestParam("image")MultipartFile image, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            return "/additions/" + option.getAddition().getId();
        }

        Addition addition = additionRepository.findById(option.getAddition().getId()).orElse(new Addition(-1L));

        if (addition.getId() < 0) {
            throw new BadRequestException("Wrong data");
        }

        String imgUri = AppConstants.NO_IMG_URI;
        if (image != null && !image.isEmpty()) {
            fileStorageService.isImage(image);
            imgUri = fileStorageService.store(image, "images" + File.separator + "options",addition.getName());
        }
        option.setImg(imgUri);
        if (null != optionService.save(option)) {
            redirectAttributes.addFlashAttribute("flash.message", "Option saved successfully!");
        } else {
            redirectAttributes.addFlashAttribute("error", "Error during saving Option.");
        }
        return "redirect:/additions/" + option.getAddition().getId();
    }


    @PostMapping("/options/edit")
    public String edit(@Valid Option option, @RequestParam("image") MultipartFile image, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "redirect:/options/" + option.getId();
        }
        Option existsOption = optionService.findOne(option.getId()).orElse(new Option(-1L));
        if (existsOption.getId() < 0) {
            throw new BadRequestException("Option not found");
        }

        String imgUri = existsOption.getImg();

        if (image != null && !image.isEmpty()) {
            fileStorageService.isImage(image);
            if (imgUri != null && !imgUri.equals(AppConstants.NO_IMG_URI) && !imgUri.startsWith("http")) {
                fileStorageService.delete(imgUri);
            }
            imgUri = fileStorageService.store(image, "images" ,existsOption.getAddition().getName());
        }
        option.setImg(imgUri);
        option.setAddition(existsOption.getAddition());
        if (optionService.save(option) != null) {
            redirectAttributes.addFlashAttribute("flash.message", "Successful");
        } else {
            redirectAttributes.addFlashAttribute("flash.error", "Operation is aborted");
        }

        return "redirect:/options/" + existsOption.getId();
    }

    @GetMapping("/options/{optionId:[\\d]+}")
    public String getOptionUi(@PathVariable(name = "optionId") Long optionId, Model model) {
        Option option = optionService.findOne(optionId).orElse(new Option(-1L));

        if (option.getId() < 0) {
            throw new BadRequestException("Option not found");
        }
        model.addAttribute("option", option);
        return "admin/option_single";
    }

    @PostMapping("/options/delete")
    public String deleteProduct(@RequestParam("id") String productId, RedirectAttributes redirectAttributes) {
        long id = -1L;
        try {
            id = Long.valueOf(productId);
        } catch (NumberFormatException e) {
            throw new BadRequestException("Wrong request parameters");
        }

        Option option = optionService.findOne(id).orElse(new Option(-1L));
        if (option.getId() < 0) {
            throw new BadRequestException("Option not found");
        }
        optionService.delete(option);
        redirectAttributes.addFlashAttribute("flash.message","Option successfully deleted!");
        return "redirect:/products";
    }

}
