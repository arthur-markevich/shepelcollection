package com.shepelcollection.shepel.service;

import com.shepelcollection.shepel.model.AjaxResponseOrder;
import com.shepelcollection.shepel.model.Order;
import com.shepelcollection.shepel.model.OrderItem;
import com.shepelcollection.shepel.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;


    public List<Order> findAll() {
        return  orderRepository.findAll();
    }

    public Order save(Order order) {
        return orderRepository.save(order);
    }

    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }


    public AjaxResponseOrder getOrderDetails(List<OrderItem> currentOrder) {
        AjaxResponseOrder result = new AjaxResponseOrder();

        BigDecimal amount = BigDecimal.ZERO;
        int itemsCount = 0;

        for (OrderItem i : currentOrder) {
            amount = amount.add(i.getPrice());
            itemsCount++;
        }
        result.setCount(itemsCount);
        result.setAmount(amount);

        return result;
    }


}
