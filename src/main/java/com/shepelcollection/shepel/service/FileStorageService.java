package com.shepelcollection.shepel.service;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface FileStorageService {

    String store(MultipartFile file, String directory, String subDirectory);

    void delete(String fileUri);

    Resource loadFile(String filename);

    void init();

    Stream<Path> loadFiles();

    boolean isImage(MultipartFile file);

}
