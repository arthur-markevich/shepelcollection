package com.shepelcollection.shepel.service;

import com.shepelcollection.shepel.exception.ResourceNotFoundException;
import com.shepelcollection.shepel.model.Role;
import com.shepelcollection.shepel.model.RoleName;
import com.shepelcollection.shepel.model.User;
import com.shepelcollection.shepel.repository.RoleRepository;
import com.shepelcollection.shepel.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;


@Service("userSirvice")
public class UserServiceIml implements UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public Optional<User> findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", RoleName.ROLE_USER));
        user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        userRepository.save(user);

    }
}
