package com.shepelcollection.shepel.service;

import com.shepelcollection.shepel.model.Option;
import com.shepelcollection.shepel.repository.OptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OptionService {

    @Autowired
    private OptionRepository optionRepository;


    public Option save(Option option) {
        return optionRepository.save(option);
    }

    public List<Option> findAll() {
        return optionRepository.findAll();
    }

    public List<Option> findAllByAdditionId(Long id) {
        return optionRepository.findAllByAddition_Id(id);
    }

    public Optional<Option> findOne(Long id) {
        return optionRepository.findById(id);
    }

    public void delete(Option option) {
        optionRepository.delete(option);
    }




}
