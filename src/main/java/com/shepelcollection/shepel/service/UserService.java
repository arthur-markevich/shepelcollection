package com.shepelcollection.shepel.service;

import com.shepelcollection.shepel.model.User;

import java.util.Optional;


public interface UserService {

    public Optional<User> findUserByEmail(String email);

    public void saveUser(User user);

}
