package com.shepelcollection.shepel.service;

import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.oreilly.servlet.multipart.FileRenamePolicy;
import com.shepelcollection.shepel.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
public class FileStorageServiceImpl implements FileStorageService {

    @Value("${file.location.path}")
    private String rootPath;

    private final static String[] imageMimeTypes = { "image/gif", "image/jpeg", "image/png", "image/pjpeg" };


    private Path rootLocation = Paths.get("/Users/Art/work/shepel/target/classes/static");



    @Override
    public String store(MultipartFile file, String directory, String subDirectory) {

        System.out.println("rootPath --> " + rootPath);

        //temporary
        if (rootPath == null) {
//            rootPath = "C:\\Users\\Diveloper\\SpringShepel\\shepelcollection\\target\\classes\\static";
            rootPath = "/Users/Art/work/shepel/target/classes/static";
        }

        // Change root location directory!
        rootLocation = Paths.get(rootPath + File.separator + directory);

        Path uploadPath = Paths.get(rootLocation.toString() + File.separator + subDirectory);
        String fileName = file.getOriginalFilename();

        try {
            if (!Files.exists(rootLocation)) {
                Files.createDirectory(rootLocation);
            }
            if (!Files.exists(uploadPath)) {
                Files.createDirectory(uploadPath);
            }

            File existsFile = new File(uploadPath.toFile(), fileName);
            System.out.println("exists file --> " + existsFile);
            if (existsFile.exists()) {
                FileRenamePolicy frp = new DefaultFileRenamePolicy();
                File tmpF = new File(fileName);
                fileName = frp.rename(tmpF).getName();
            }

            Files.copy(file.getInputStream(), uploadPath.resolve(fileName));
            return directory + "/" + subDirectory + "/" + fileName;

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("FAIL --> message = " + e.getMessage());
        }
    }

    @Override
    public void delete(String fileUri) {
        String compatibleFileUri = fileUri.replace("/", File.separator);
        Path filePath = Paths.get(rootPath + File.separator + compatibleFileUri);
        if (Files.exists(filePath)) {
            try {
                Files.delete(filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Resource loadFile(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error --> message = " + e.getMessage());
        }
    }

    @Override
    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    @Override
    public Stream<Path> loadFiles() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read store file");
        }
    }

    @Override
    public boolean isImage(MultipartFile file) {
        String mimeType = file.getContentType();
        if (mimeType == null) {
            throw new BadRequestException("Only image uploading allowed! mime-type: " + mimeType);
        }
        for (String s: imageMimeTypes) {
            if (s.equals(mimeType)) {
                return true;
            }
        }
        throw new BadRequestException("Only image uploading allowed! mime-type: " + mimeType);

    }
}
