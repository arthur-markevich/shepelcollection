package com.shepelcollection.shepel.service;

public class HelperService {

    public static Long getValue(String id) {
        if (id == null) {
            return -1L;
        }
        try {
            return Long.valueOf(id);
        } catch (NumberFormatException ex) {
            System.out.println("Not a number: " + id);
        }

        return -1L;
    }
}
