package com.shepelcollection.shepel.service;

import com.shepelcollection.shepel.config.AppConstants;
import com.shepelcollection.shepel.model.Product;
import com.shepelcollection.shepel.repository.CategoryRepository;
import com.shepelcollection.shepel.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    FileStorageService fileStorageService;

    private static final BigDecimal TEN_HUNDREDS = new BigDecimal(1000);



    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public List<Product> findAllByCategoryId(Long id) {
        return productRepository.findAllByCategoryId(id);
    }

    public List<Product> findAllByCategory(String name) {
        return productRepository.findAllByCategoryName(name);
    }

    public Optional<Product> findOne(Long id) {
        return productRepository.findById(id);
    }

    public void delete(Product product) {

        if (!product.getImg().equals(AppConstants.NO_IMG_URI) && !product.getImg().startsWith("http")) {
            System.out.println("imgUri ---> " + product.getImg());
            fileStorageService.delete(product.getImg());
        }
        productRepository.delete(product);
    }


    public void saveProduct(Product product) {
        product.setVolume(new BigDecimal(product.getHeight()).divide(TEN_HUNDREDS).multiply(new BigDecimal(product.getWidth()).divide(TEN_HUNDREDS))
                .multiply(new BigDecimal(product.getDepth()).divide(TEN_HUNDREDS)));

        Instant now = Instant.now();

        product.setCreatedAt(now);
        product.setUpdatedAt(now);

        productRepository.save(product);
    }
}
