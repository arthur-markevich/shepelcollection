package com.shepelcollection.shepel.service;

import com.shepelcollection.shepel.model.Addition;
import com.shepelcollection.shepel.model.Type;
import com.shepelcollection.shepel.repository.AdditionRepository;
import com.shepelcollection.shepel.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TypeService {

    @Autowired
    private TypeRepository typeRepository;



    public Optional<Type> findById(Long id) {
        return typeRepository.findById(id);
    }


    public List<Addition> getAllLeftAdditions(Type type, List<Addition> allAdds) {
        List left = new ArrayList(allAdds);
        left.removeAll(type.getAdditions());
        return left;
    }
}
