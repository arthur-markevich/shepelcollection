package com.shepelcollection.shepel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShepelApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShepelApplication.class, args);
	}


}
