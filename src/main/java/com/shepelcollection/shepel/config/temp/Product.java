package com.shepelcollection.shepel.config.temp;

import java.math.BigDecimal;
import java.util.Date;

public class Product {

    private Long id;

    private String name;

    private String img;

    private Date createdAt;

    private Date updatedAt;

    private BigDecimal priceFabric;
    private BigDecimal priceSuede;
    private Long height;
    private Long width;
    private Long depth;
    private Long volume;

    private Item item;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public BigDecimal getPriceFabric() {
        return priceFabric;
    }

    public void setPriceFabric(BigDecimal priceFabric) {
        this.priceFabric = priceFabric;
    }

    public BigDecimal getPriceSuede() {
        return priceSuede;
    }

    public void setPriceSuede(BigDecimal priceSuede) {
        this.priceSuede = priceSuede;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getDepth() {
        return depth;
    }

    public void setDepth(Long depth) {
        this.depth = depth;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}