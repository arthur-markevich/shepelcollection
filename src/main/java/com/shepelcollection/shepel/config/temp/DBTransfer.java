package com.shepelcollection.shepel.config.temp;

import org.thymeleaf.model.ITemplateEnd;

import java.math.BigDecimal;
import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DBTransfer {

    private static final String URL_FROM = "jdbc:mysql://localhost/shepelcollection" + "?user=root&password=root";

    private static final String URL_TO = "jdbc:mysql://localhost/springshepel" + "?user=root&password=root";

    private static final Pattern pat = Pattern.compile("\\d{2,}\\.*\\d*");

    private static List<Item> items;

    private static List<Product> products;


    public static void main(String[] args) {
        load();
//        System.out.println("products size --> " + products.size());
//        Set<String> codes = new HashSet<>();
//        for (Product p : products) {
//            codes.add(getCode(p.getName()));
//        }
//        System.out.println("unique codes --> " + codes.size());
        upload();
    }

    private static String getCode(String name) {
        Matcher mat;
        mat = pat.matcher(name);

        if (mat.find()) {
            return mat.group();
        }
        return null;
    }

    private static String getName(String name) {
        return  name.split("\\d{2,}\\.*\\d*")[0];
    }

    public static void upload() {
        try (Connection con = DriverManager.getConnection(URL_TO)) {
            con.setAutoCommit(false);

            try (PreparedStatement pstm = con.prepareStatement("INSERT INTO category VALUES (?, ?)")) {
                for (Item i : items) {
                    pstm.setLong(1, i.getId());
                    pstm.setString(2, i.getName());
                    pstm.addBatch();
                }
                pstm.executeBatch();
                pstm.clearBatch();
            }

            try (PreparedStatement ps = con.prepareStatement("INSERT INTO product VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
                BigDecimal southend = new BigDecimal(1000);
                for(Product p: products) {
                    int k = 1;

                    ps.setLong(k++, p.getId());
                    ps.setTimestamp(k++, Timestamp.from(Instant.now()));
                    ps.setTimestamp(k++, Timestamp.from(Instant.now()));
                    ps.setString(k++, getCode(p.getName()));
                    ps.setLong(k++, p.getDepth());
                    ps.setLong(k++, p.getHeight());
                    ps.setString(k++, p.getImg());
                    ps.setString(k++, getName(p.getName()));
                    ps.setBigDecimal(k++, p.getPriceFabric());
                    ps.setBigDecimal(k++, p.getPriceSuede());
                    ps.setBigDecimal(k++, new BigDecimal(p.getHeight()).divide(southend).multiply(new BigDecimal(p.getWidth()).divide(southend))
                            .multiply(new BigDecimal(p.getDepth()).divide(southend)));
                    ps.setLong(k++, p.getWidth());
                    ps.setLong(k++, p.getItem().getId());
                    ps.setLong(k++, 1L);
                    ps.addBatch();
                }
                ps.executeBatch();



                con.commit();
            } catch (SQLException e) {
                con.rollback();
                throw new SQLException(e);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public static void load() {
        items = new ArrayList<>();
        products = new ArrayList<>();

        try (Connection conFrom = DriverManager.getConnection(URL_FROM);
             Statement stm = conFrom.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM Items")) {
                while (rs.next()) {
                    Item item = new Item(rs.getLong("id"), rs.getString("name"));
                    items.add(item);
                }
            }

            try (ResultSet rs2 = stm.executeQuery("SELECT  * FROM Products")) {
                while (rs2.next()) {
                    Product product = new Product();
                    product.setId(rs2.getLong("id"));
                    product.setName(rs2.getString("name"));
                    product.setImg(rs2.getString("img"));
                    product.setPriceFabric(rs2.getBigDecimal("price_fabric"));
                    product.setPriceSuede(rs2.getBigDecimal("price_suede"));
                    product.setHeight(rs2.getLong("heigth"));
                    product.setWidth(rs2.getLong("width"));
                    product.setDepth(rs2.getLong("depth"));
                    product.setItem(new Item(rs2.getLong("itemId")));

                    products.add(product);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
