package com.shepelcollection.shepel.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

//@SpringBootTest
//@SpringBootConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
class FileStorageServiceImplTest {


    private static FileStorageService fileStorageService;

//    @Value("${tmp.file.location}")
    private String tmpFilePath = "C:\\Users\\Diveloper\\Downloads\\tmp.png";

//    @Value("${file.location.path}")
    private String rootPath ="C:\\Users\\Diveloper\\SpringShepel\\shepelcollection\\target\\classes\\static";


    @BeforeAll
    public static void loadTest() {
        fileStorageService = new FileStorageServiceImpl();
    }



    @org.junit.jupiter.api.Test
    void delete() throws IOException {
        System.out.println("tmpFilePath --> " + tmpFilePath);
        System.out.println("rootPath --> " + rootPath);

        FileInputStream inputFile = new FileInputStream(tmpFilePath);
        MockMultipartFile file = new MockMultipartFile("file", "testTmpFile", "multipart/form-data", inputFile);

        String storedFilePath = fileStorageService.store(file, "test", "tmp");

        System.out.println("Stored file Path --> " + storedFilePath);

        if (storedFilePath != null) {
            String compatibleFileUri = storedFilePath.replace("/", File.separator);
            Path filePath = Paths.get(rootPath + File.separator + compatibleFileUri);
            assertTrue(Files.exists(filePath));

            fileStorageService.delete(storedFilePath);
            assertFalse(Files.exists(filePath));
        }

    }

    @Configuration
    @ComponentScan("com.shepelcollection.shepel.service")
    static class LaodProperties {
        @Bean
        PropertyPlaceholderConfigurer  propConfig() {
            PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer ();
            ppc.setLocation(new ClassPathResource("application.properties"));
            return ppc;
        }
    }
}